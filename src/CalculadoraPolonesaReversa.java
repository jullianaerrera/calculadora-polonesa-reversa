
public class CalculadoraPolonesaReversa {

	private static double[] pilha = new double[10];
	private static int posicao = 0;

	public static void main(String[] args) {
		adicionar(2);
		adicionar(3);
		adicionar(8);
		adicionar(5);
		somar();
		adicionar(1);
		adicionar(4);
		adicionar(7);
		adicionar(6);
		subtrair();
		adicionar(5);
		adicionar(4);
		multiplicar();
		adicionar(10);
		adicionar(2);
		adicionar(5);
		divisao();
		imprimir();
	}

	private static void adicionar(double adicionar) {
		pilha[posicao] = adicionar;
		posicao = posicao + 1;
	}

	private static void somar() {
		double resultado = pilha[posicao - 1] + pilha[posicao - 2];
		controlarPilha(resultado);
	}

	private static void subtrair() {
		double resultado = pilha[posicao - 1] - pilha[posicao - 2];
		controlarPilha(resultado);
	}

	private static void multiplicar() {
		double resultado = pilha[posicao - 1] * pilha[posicao - 2];
		controlarPilha(resultado);
	}

	private static void divisao() {
		double resultado = pilha[posicao - 1] / pilha[posicao - 2];
		controlarPilha(resultado);
	}

	private static void controlarPilha(double resultado) {
		pilha[posicao - 1] = 0;
		pilha[posicao - 2] = resultado;
		posicao = posicao - 1;
	}

	private static void imprimir() {
		for (int i = 0; i <= 9; i++) {
			System.out.println(pilha[i]);
		}
	}
	
}
